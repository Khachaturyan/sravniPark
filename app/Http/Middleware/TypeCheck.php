<?php

namespace App\Http\Middleware;

use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Http\Request;

class TypeCheck
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->hasHeader('Type') && ($request->header('Type') === 'park' || $request->header('Type') === 'driver')) {
            return $next($request);
        }else{
            return $this->getFailResponse('missing_or_wrong_type');
        }
    }
}
