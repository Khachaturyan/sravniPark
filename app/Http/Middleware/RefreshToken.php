<?php

namespace App\Http\Middleware;

use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Http\Request;

class RefreshToken
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->hasHeader('RefreshToken') && !empty($request->header('RefreshToken'))){
            return $next($request);
        }else{
            return $this->getFailResponse('missing_refresh_token');
        }

    }
}
