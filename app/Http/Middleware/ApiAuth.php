<?php

namespace App\Http\Middleware;

use App\Models\Driver;
use App\Services\ApiAuthMiddlewareService;
use App\Traits\ResponseTrait;
use Closure;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;

class ApiAuth
{
    use ResponseTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!$request->hasHeader('Authorization')) {
            return $this->getFailResponse('missing_authorisation_token');
        } else {
            $api_middleware_serv = new ApiAuthMiddlewareService($request);
            $check = $api_middleware_serv->check();
            if ($check) {
                return $next($request);
            } else {
                return $this->getFailResponse("wrong_api_token_or_expired");
            }
        }
    }
}
