<?php

namespace App\Http\Controllers\Api\Park;

use App\Http\Controllers\Controller;
use App\Models\ParkSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ParkSettingsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tech_support' => 'required|boolean',
            'car_rent' => 'required|boolean',
            'office' => 'required|boolean',
            'commission'=>'required|numeric'

        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->all();

        $park = Auth::user();

        ParkSetting::updateOrCreate([
            'park_id' => $park->id
        ], [
            'tech_support' => $data['tech_support'],
            'car_rent' => $data['car_rent'],
            'office' => $data['office'],
            'commission' => $data['commission'],
        ]);

        return $this->getSuccessResponse('ok',$park->park_full_data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCities(Request $request){
        $validator = Validator::make($request->all(), [
            'cities' => 'required|array',
            'cities.*'=>'exists:cities,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $data = $request->all();

        $park = Auth::user();

        $park->cities()->sync($data['cities']);

        return $this->getSuccessResponse('ok',$park->park_full_data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateAggregators(Request $request){
        $validator = Validator::make($request->all(), [
            'aggregators' => 'required|array',
            'aggregators.*'=>'exists:aggregators,id'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $data = $request->all();

        $park = Auth::user();

        $park->aggregators()->sync($data['aggregators']);

        return $this->getSuccessResponse('ok',$park->park_full_data);

    }
}
