<?php

namespace App\Http\Controllers\Api\Park;

use App\Http\Controllers\Controller;
use App\Models\ParkAnnouncement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ParkAnnouncementController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $park = Auth::user();
        $park_announcement = ParkAnnouncement::where('park_id',$park->id)->get();
        return $this->getSuccessResponse('ok',$park_announcement->load(['park','drivers']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'announcement' => 'required',
            'draft'=>'required|boolean'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $data = $request->all();

        $park = Auth::user();
        $ret = $park->parkAnnouncements()->create([
            'announcement'=>$data['announcement'],
            'draft'=>$data['draft']
        ]);

        return $this->getSuccessResponse('ok',$ret);

    }

    /**
     * @param $announcement_id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($announcement_id,Request $request){
        $validator = Validator::make($request->all(), [
            'announcement' => 'required',
            'draft'=>'required|boolean'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $data = $request->all();
        $parkAnnouncement = ParkAnnouncement::find($announcement_id);

       $parkAnnouncement->update([
            'announcement'=>$data['announcement'],
            'draft'=>$data['draft']
        ]);

        return $this->getSuccessResponse('ok',$parkAnnouncement);
    }


    /**
     * @param $announcement_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($announcement_id){

        $parkAnnouncement = ParkAnnouncement::find($announcement_id);
        if(is_null($parkAnnouncement)){
            return $this->getFailResponse('no such announcement');
        }
        $parkAnnouncement->drivers()->detach();
        $parkAnnouncement->delete();
        return $this->getSuccessResponse('deleted successfully');
    }

    public function getParkAnnouncementDriver(){
        $pa = ParkAnnouncement::all();
        return $this->getSuccessResponse('ok',$pa->load(['park']));
    }
}
