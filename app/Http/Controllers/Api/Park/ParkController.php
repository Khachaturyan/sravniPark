<?php

namespace App\Http\Controllers\Api\Park;

use App\Http\Controllers\Controller;
use App\Models\Park;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ParkController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $parks = Park::all();
        return $this->getSuccessResponse('ok', $parks->load(['parkSetting','cities','aggregators']));
    }

    public function show(){
        $park = Auth::user();
        return $this->getSuccessResponse('ok',$park->park_full_data);
    }

    public function showToDriver(Park $park){
        return $this->getSuccessResponse('ok',$park->park_full_data);
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->all();

        $park = Auth::user();
        $park->update([
            'name' => $data['name']
        ]);

        return $this->getSuccessResponse('ok', $park->park_full_data);
    }

    public function updateImage(Request $request){
        $validator = Validator::make($request->all(), [
            'photo' => 'is_jpeg'
        ], ['is_jpeg' => 'photo must be base64 jpeg without this part -> \'data:image/jpeg;base64,\' ']);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $park = Auth::user();
        $park->update([
            'photo' => $request->photo
        ]);

        return $this->getSuccessResponse('ok', $park->park_full_data);

    }
}
