<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\DriverExperienceClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DriverExperienceController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $driverExperience = DriverExperienceClass::all();
        return $this->getSuccessResponse('ok',$driverExperience);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'experience_ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $experience_ids = $request->experience_ids;

        $driver = Auth::user();
        $driver->driverSettings->driverExperienceClass()->sync($experience_ids);

        return $this->getSuccessResponse('ok', $driver->driver_full_data);
    }
}
