<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DriverController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $drivers = Driver::with(['driverSettings' => function ($q) {
            $q->with('driverExperienceClass', 'driverLicenseClass');
        }])->get();
        return $this->getSuccessResponse('ok', $drivers);
    }

    /**
     * @param Driver $driver
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Driver $driver)
    {
        $driver = $driver->driver_full_data;
        return $this->getSuccessResponse('ok', $driver);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'middle_name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->all();

        $driver = Auth::user();
        $driver->update([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'middle_name' => $data['middle_name']
        ]);

        return $this->getSuccessResponse('ok', $driver->driver_full_data);
    }

    public function updateImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'photo' => 'is_jpeg'
        ], ['is_jpeg' => 'photo must be base64 jpeg without this part -> \'data:image/jpeg;base64,\' ']);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $driver = Auth::user();
        $driver->update([
            'photo' => $request->photo
        ]);

        return $this->getSuccessResponse('ok', $driver->driver_full_data);
    }
}
