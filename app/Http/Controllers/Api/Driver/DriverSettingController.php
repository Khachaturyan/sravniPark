<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\DriverSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DriverSettingController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'citizen' => 'required',
            'driver_license_city' => 'required',
            'driver_experience' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }
        $data = $request->all();

        $driver = Auth::user();

        DriverSetting::updateOrCreate([
            'driver_id' => $driver->id
        ], [
            'citizen' => $data['citizen'],
            'driver_license_city' => $data['driver_license_city'],
            'driver_experience' => $data['driver_experience'],
        ]);

        return $this->getSuccessResponse('ok', $driver->driver_full_data);
    }
}
