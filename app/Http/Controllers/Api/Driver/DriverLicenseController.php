<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\DriverExperienceClass;
use App\Models\DriverLicenseClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DriverLicenseController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $driverLicenses = DriverLicenseClass::all();
        return $this->getSuccessResponse('ok',$driverLicenses);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'license_ids' => 'required|array',
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $license_ids = $request->license_ids;


        $driver = Auth::user();
        $driver->driverSettings->driverLicenseClass()->sync($license_ids);

        return $this->getSuccessResponse('ok', $driver->driver_full_data);
    }
}
