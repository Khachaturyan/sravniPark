<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use App\Models\Rate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DriverActionController extends Controller
{
    public function responseToAnnouncement($announcement_id)
    {
        $driver = Auth::user();
        $driver->parkAnnouncements()->sync([$announcement_id]);

        return $this->getSuccessResponse('ok');
    }

    public function ratePark(Request $request){
        $validator = Validator::make($request->all(), [
            'park_id' => 'required|exists:parks,id',
            'rate'=>'numeric|min:1|max:10'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->all();
        $driver = Auth::user();
        $check = Rate::where('driver_id',$driver->id)->where('park_id',$data['park_id'])->get();

        if($check->count() > 0){
            return $this->getFailResponse('driver already rate this park');
        }

        Rate::create([
            'driver_id'=>$driver->id,
            'park_id'=>$data['park_id'],
            'rate'=>$data['rate']
        ]);

        return $this->getSuccessResponse('ok');
    }
}
