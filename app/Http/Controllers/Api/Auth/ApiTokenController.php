<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\Park;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use \Firebase\JWT\JWT;

class ApiTokenController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|digits:11',
            'sms' => 'required|exists:sms_tokens,code|digits:4'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }


        $data = $request->only(['phone', 'sms']);
        $phone = $data['phone'];
        $sms = $data['sms'];

        switch ($request->header('Type')) {
            case 'driver':
                $user = Driver::where('phone', '=', $phone)
                    ->first();
                break;
            case 'park':
                $user = Park::where('phone', '=', $phone)
                    ->first();
                break;
        }

        $smsTokens = $user->smsToken();
        $smsTokens->where('send_at', '<', (now()->addMinutes(-10)))
            ->update(['expired' => true]);

        $smsTokens = $user->smsToken;
        $check = $smsTokens->where('code', $sms);

        if ($check->count() === 0) {
            return $this->getFailResponse('wrong_sms_code');
        }

        $check = $check->first();
        if ($check->used == true || $check->expired == true) {
            return $this->getFailResponse('sms_code_expired');
        } elseif ($check->used == false && $check->expired == false) {

            $api_token = Str::random(60);
            $refresh_token = Str::random(60);

            $key = env('JWT_KEY');
            $payload = array(
                "user_id" => $user->id,
                "token" => $api_token
            );

            $refresh_payload = array(
                "user_id" => $user->id,
                "token" => $refresh_token
            );

            $jwt_api_token = JWT::encode($payload, $key);
            $jwt_refresh_token = JWT::encode($refresh_payload, $key);


            $user->api_token = Hash::make($api_token);
            $user->refresh_token = Hash::make($refresh_token);

            $user->api_token_created_at = now();
            $user->save();

            $check->used = true;

            $check->save();

            $returnData = [
                'api_token' => $jwt_api_token,
                'refresh_token' => $jwt_refresh_token
            ];

            return $this->getSuccessResponse('ok', $returnData);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $key = env('JWT_KEY');
        $jwt = $request->header('RefreshToken');

        $payload = (array)JWT::decode($jwt, $key, array('HS256'));

        $user_id = $payload['user_id'];
        $token = $payload['token'];

        switch ($request->header('Type')) {
            case 'driver':
                $user = Driver::find($user_id);
                break;
            case 'park':
                $user = Park::find($user_id);
                break;
        }

        $refresh_token = $user->refresh_token;

        if (!Hash::check($token, $refresh_token)) {
            return $this->getFailResponse("invalid_refresh_token");
        }

        $api_token = Str::random(60);

        $new_payload = array(
            "user_id" => $user->id,
            "token" => $api_token
        );

        $jwt_api_token = Jwt::encode($new_payload, $key);
        $user->api_token = Hash::make($api_token);
        $user->api_token_created_at = now();
        $user->save();

        $returnData = [
            'api_token' => $jwt_api_token
        ];

        return $this->getSuccessResponse('ok', $returnData);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $jwt = $request->bearerToken();
        $key = env("JWT_KEY");
        $payload = (array)JWT::decode($jwt, $key, array('HS256'));
        $user_id = $payload["user_id"];

        switch ($request->header('Type')) {
            case 'driver':
                $user = Driver::find($user_id);
                break;
            case 'park':
                $user = Park::find($user_id);
                break;
        }

        $user->api_token = null;
        $user->refresh_token = null;
        $user->api_token_created_at = null;
        $user->save();

        return $this->getSuccessResponse("successfully_logout");
    }

}
