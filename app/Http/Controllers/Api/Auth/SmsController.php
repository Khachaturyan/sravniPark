<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\Park;
use App\Models\SmsToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SmsController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSmsLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|digits:11'
        ]);

        if ($validator->fails()) {
            return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
        }

        $data = $request->only(['phone']);

        $phone = $data['phone'];


        switch ($request->header('Type')) {
            case 'driver':
                $model = Driver::where('phone', $phone)->get();
                break;
            case 'park':
                $model = Park::where('phone', $phone)->get();
                break;
        }
        if ($model->count() > 0) {
            $retVal = $this->generateAndSendSms($model->first());
            if ($retVal['success']) {
                return $this->getSuccessResponse($retVal['message']);
            } else {
                return $this->getFailResponse((string)$retVal['message']);
            }
        } else {
            return $this->getFailResponse('wrong_phone_number');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSms(Request $request)
    {
        switch ($request->header('Type')) {
            case 'driver':
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|digits:11',
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'middle_name' => 'required'
                ]);

                if ($validator->fails()) {
                    return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
                }

                $data = $request->only(['phone', 'first_name', 'last_name', 'middle_name']);

                $phone = $data['phone'];
                $first_name = $data['first_name'];
                $last_name = $data['last_name'];
                $middle_name = $data['middle_name'];

                $driver = Driver::firstOrCreate(
                    [
                        'phone' => $phone
                    ],
                    [
                        'first_name' => $first_name,
                        'last_name' => $last_name,
                        'middle_name' => $middle_name
                    ]
                );
                $retVal = $this->generateAndSendSms($driver);
                if ($retVal['success']) {
                    return $this->getSuccessResponse($retVal['message']);
                } else {
                    return $this->getFailResponse((string)$retVal['message']);
                }
                break;

            case 'park':
                $validator = Validator::make($request->all(), [
                    'phone' => 'required|digits:11',
                    'name' => 'required',
                    'email' => 'required',
                    'inn' => 'required|numeric|digits:12',
                    'aggregators' => 'required|array',
                    'aggregators.*' => 'exists:aggregators,id',
                    'cities' => 'required|array|exists:cities,id',
                    'cities.*' => 'exists:cities,id'
                ]);

                if ($validator->fails()) {
                    return $this->getFailResponse("validation_fail", $validator->getMessageBag()->getMessages());
                }

                $data = $request->all();

                $phone = $data['phone'];
                $name = $data['name'];
                $email = $data['email'];
                $inn = $data['inn'];

                $park = Park::firstOrCreate(
                    [
                        'phone' => $phone,
                        'email' => $email,
                        'inn' => $inn
                    ],
                    [
                        'name' => $name,
                    ]
                );

                $park->aggregators()->sync($data['aggregators']);
                $park->cities()->sync($data['cities']);

                $retVal = $this->generateAndSendSms($park);
                if ($retVal['success']) {
                    return $this->getSuccessResponse($retVal['message']);
                } else {
                    return $this->getFailResponse((string)$retVal['message']);
                }
                break;
        }

    }

    /**
     * @param $user
     * @return array
     */
    public function generateAndSendSms($user)
    {
        $smsTokens = $user->smsToken;
        foreach ($smsTokens as $smsToken) {
            if ($smsToken->send_at->diffInMinutes(now()) < 2) {
                return ['success' => false, 'message' => 'cant_send_sms_more_than_2_minutes'];
            } elseif ($smsToken->send_at->diffInMinutes(now()) > 2 && $smsToken->send_at->diffInMinutes(now()) < 10) {
                $smsToken->expired = true;
                $smsToken->save();
            }
        }

        $code = SmsToken::generateCode();

        //todo find sms service and write it here

        if (1 == 1) {//on success sms service
            SmsToken::create([
                'sms_tokenables_id' => $user->id,
                'sms_tokenables_type' => "App\Models\\" . class_basename($user),
                'code' => $code,
                'send_at' => now()
            ]);
            return ['success' => true, 'message' => 'ok'];
        } else {
            return ['success' => false, 'message' => 'sms_not_send'];
        }
    }
}
