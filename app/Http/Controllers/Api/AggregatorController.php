<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Aggregator;
use Illuminate\Http\Request;

class AggregatorController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $aggregators = Aggregator::all();
        return $this->getSuccessResponse('ok',$aggregators);
    }
}
