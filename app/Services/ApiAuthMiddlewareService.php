<?php

namespace App\Services;

use App\Models\Driver;
use App\Models\Park;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Hash;

class ApiAuthMiddlewareService {

    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function check(){

        $value = $this->request->header('Type');
        if($value === 'driver'){
            $model = new Driver();
        }elseif($value === 'park'){
            $model = new Park();
        }
        $jwt = $this->request->bearerToken();
        $key = env("JWT_KEY");

        $payload = (array)JWT::decode($jwt, $key, array('HS256'));

        $user_id = $payload["user_id"];
        $token = $payload["token"];

        $user = $model->find($user_id);
        if(is_null($user)){
            return false;
        }

        $api_token = $user->api_token;
        $api_token_created_at = $user->api_token_created_at;

        $token_expired = false;
        if ($api_token_created_at->diffInMinutes(now()) > 30) {
            $token_expired = true;
        }

        if(Hash::check($token, $api_token) && $token_expired == false){
            return true;
        }else{
            return false;
        }

    }
}
