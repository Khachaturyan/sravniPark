<?php

namespace App\Services;

use App\Models\Driver;
use App\Models\Park;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Request;

class ParkGuard implements Guard
{

    protected $provider;
    protected $user;

    public function __construct(UserProvider $provider)
    {
        $this->provider = $provider;
        $this->user = NULL;

    }

    public function user()
    {

        if (!is_null($this->user)) {
            return $this->user;
        }

        if (is_null(Request::bearerToken())) {
            return $this->user;
        }

        $jwt = Request::bearerToken();
        $key = env("JWT_KEY");

        $payload = (array)JWT::decode($jwt, $key, array('HS256'));

        $park_id = $payload["user_id"];

        $this->user = Park::find($park_id);

        return $this->user;
    }

    public function id()
    {
        if ($user = $this->user()) {
            return $this->user()->getAuthIdentifier();
        }
    }

    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
        return $this;
    }

    public function guest()
    {
        return !$this->check();
    }

    public function check()
    {
        return !is_null($this->user());
    }

    public function validate(array $credentials = [])
    {
        return true;
    }
}
