<?php

namespace App\Services;

use App\Models\Driver;
use App\Models\Park;
use Firebase\JWT\JWT;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Request;

class DriverGuard implements Guard
{

    protected $provider;
    protected $user;

    public function __construct(UserProvider $provider)
    {
        $this->provider = $provider;
        $this->user = NULL;

    }

    public function user()
    {
        if (!is_null($this->user)) {
            return $this->user;
        }

        if (is_null(Request::bearerToken())) {
            return $this->user;
        }


        $jwt = Request::bearerToken();
        $key = env("JWT_KEY");

        $payload = (array)JWT::decode($jwt, $key, array('HS256'));

        $user_id = $payload["user_id"];
        switch(Request::header('Type')){
            case 'park':
                $this->user = Park::find($user_id);
                break;
            case 'driver':
                $this->user = Driver::find($user_id);
                break;
        }


        return $this->user;
    }

    public function checkDriver($driver_id)
    {
        $driver_user = $this->user;

        $drivers = $driver_user->drivers()
            ->where('id', $driver_id)
            ->get();
        if ($drivers->count() > 0) {
            return true;
        }
        return false;
    }

    public function id()
    {
        if ($user = $this->user()) {
            return $this->user()->getAuthIdentifier();
        }
    }

    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
        return $this;
    }

    public function guest()
    {
        return !$this->check();
    }

    public function check()
    {
        return !is_null($this->user());
    }

    public function validate(array $credentials = [])
    {
        return true;
    }
}
