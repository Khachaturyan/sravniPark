<?php

namespace App\Providers;

use App\Extensions\DriverProvider;
use App\Extensions\ParkProvider;
use App\Services\DriverGuard;
use App\Services\ParkGuard;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //

        //
        Auth::extend('driver', function ($app, $name, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\Guard...
            return new DriverGuard(Auth::createUserProvider($config['provider']));
        });

//        Auth::extend('park', function ($app, $name, array $config) {
//            // Return an instance of Illuminate\Contracts\Auth\Guard...
//            return new ParkGuard(Auth::createUserProvider($config['provider']));
//        });



        Auth::provider('driver', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...

            return new DriverProvider($app->make('\App\Models\Driver'));
        });
//        Auth::provider('park', function ($app, array $config) {
//            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
//
//            return new ParkProvider($app->make('\App\Models\Park'));
//        });
    }
}
