<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParkSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'park_id',
        'tech_support',
        'car_rent',
        'office',
        'commission'
    ];

    public function park(){
        return $this->belongsTo(Park::class);
    }
}

