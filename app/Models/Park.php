<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class Park extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name','email','phone','inn','photo'
    ];

    protected $dates = [
        'api_token_created_at',
    ];
    protected $hidden = [
        'api_token','api_token_created_at','refresh_token'
    ];

    protected $appends = ['rate','canRate'];

    public function smsToken()
    {
        return $this->morphMany('App\Models\SmsToken', 'sms_tokenables');
    }

    public function aggregators(){
        return $this->belongsToMany(Aggregator::class);
    }

    public function cities(){
        return $this->belongsToMany(City::class);
    }

    public function parkSetting(){
        return $this->hasOne(ParkSetting::class);
    }

    public function getParkFullDataAttribute(){
        return $this->load(['parkSetting','cities','aggregators']);
    }

    public function parkAnnouncements(){
        return $this->hasMany(ParkAnnouncement::class);
    }

    public function rates(){
        return  $this->hasMany(Rate::class);
    }

    public function getRateAttribute(){
        if($this->rates->count()>0){
            return $this->rates->sum('rate')/$this->rates->count();
        }else{
            return 0;
        }
    }

    public function getCanRateAttribute(){
        if(get_class(Auth::user()) === 'App\Models\Driver'){
            $driver = Auth::user();
            $check = $this->rates->where('driver_id',$driver->id);
            if($check->count() > 0){
                return false;
            }else{
                return true;
            }
        }
        return null;
    }
}
