<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Driver extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name','middle_name','last_name','phone','photo'
    ];
    protected $dates = [
        'api_token_created_at',
    ];
    protected $hidden = [
        'api_token','api_token_created_at','refresh_token'
    ];

    public function driverSettings(){
        return $this->hasOne(DriverSetting::class);
    }

    public function smsToken()
    {
        return $this->morphMany('App\Models\SmsToken', 'sms_tokenables');
    }

    public function getFullNameAttribute()
    {
        return "{$this->last_name} {$this->name} {$this->middle_name}";
    }


    public function getDriverFullDataAttribute()
    {
        return $this->load(['driverSettings'=>function($q){
            $q->with('driverExperienceClass','driverLicenseClass');
        }]);
    }

    public function parkAnnouncements(){
        return $this->belongsToMany(ParkAnnouncement::class);
    }
}
