<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'citizen',
        'driver_license_city',
        'driver_experience',
        'driver_id'
    ];

    public function driver(){
        return $this->belongsTo(Driver::class);
    }

    public function driverExperienceClass(){
        return $this->belongsToMany(DriverExperienceClass::class);
    }

    public function driverLicenseClass(){
        return $this->belongsToMany(DriverLicenseClass::class);
    }
}
