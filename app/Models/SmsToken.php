<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'used',
        'sms_tokenables_id',
        'sms_tokenables_type',
        'send_at'
    ];

    protected $dates = [
        'send_at',
    ];

    public function smsTokenables()
    {
        return $this->morphTo();
    }

    /**
     * Generate a six digits code
     *
     * @param int $codeLength
     * @return string
     */
    public static function generateCode($codeLength = 3)
    {
        $min = pow(10, $codeLength);
        $max = $min * 10 - 1;
        $code = mt_rand($min, $max);

        return $code;
    }
}
