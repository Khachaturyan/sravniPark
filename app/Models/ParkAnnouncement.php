<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParkAnnouncement extends Model
{
    use HasFactory;

    protected $fillable = [
        'announcement','park_id','draft'
    ];

    protected $hidden = [
        'updated_at'
    ];


    public function park(){
        return $this->belongsTo(Park::class);
    }

    public function drivers(){
        return $this->belongsToMany(Driver::class);
    }
}
