<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    protected $data = [
        ['city' => 'Москва'],
        ['city' => 'Екатеринбург'],
        ['city' => 'Санкт-Петербург'],
        ['city' => 'Волгоград'],
        ['city' => 'Казань'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('city');
        });

        foreach ($this->data as $d) {
            \App\Models\City::create([
                'city' => $d['city']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
