<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAggregatorsTable extends Migration
{
    private $data = [
        ['name' => 'ЯндексGo'],
        ['name' => 'DiDi'],
        ['name' => 'CityMobil'],
        ['name' => 'Gett'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aggregators', function (Blueprint $table) {
            $table->id();
            $table->string('name');
        });
        foreach ($this->data as $d) {
            \App\Models\Aggregator::create([
                'name' => $d['name']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aggregators');
    }
}
