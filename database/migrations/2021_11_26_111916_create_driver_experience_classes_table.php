<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverExperienceClassesTable extends Migration
{

    protected $data = [
        ['name' => 'Эконом'],
        ['name' => 'Конфорт'],
        ['name' => 'Комфорт +'],
        ['name' => 'Business'],
        ['name' => 'Premier'],
        ['name' => 'Elite'],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_experience_classes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        foreach ($this->data as $d){
            \App\Models\DriverExperienceClass::create([
                'name'=>$d['name']
            ]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_experience_classes');
    }
}
