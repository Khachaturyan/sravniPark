<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('inn')->unique();
            $table->string('phone')->unique();
            $table->text('photo')->nullable();
            $table->timestamps();

            $table->string('api_token', 80)
                ->unique()
                ->nullable()
                ->default(null);

            $table->timestamp('api_token_created_at', 0)->nullable();

            $table->string('refresh_token', 80)
                ->unique()
                ->nullable()
                ->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parks');
    }
}
