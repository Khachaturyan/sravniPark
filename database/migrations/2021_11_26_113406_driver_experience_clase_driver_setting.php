<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DriverExperienceClaseDriverSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_experience_class_driver_setting', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('driver_setting_id');
            $table->bigInteger('driver_experience_class_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_experience_class_driver_setting');
    }
}
