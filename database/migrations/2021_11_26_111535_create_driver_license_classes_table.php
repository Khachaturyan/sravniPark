<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverLicenseClassesTable extends Migration
{
    protected $data = [
        [
            'class' => 'A',
            'description' => 'Мотоциклы'
        ],
        [
            'class' => 'B',
            'description' => 'Легковые авто'
        ],
        [
            'class' => 'C1',
            'description' => 'Средние грузовики'
        ],
        [
            'class' => 'C',
            'description' => 'Грузовики'
        ],
        [
            'class' => 'D1',
            'description' => 'Небольшие автобусы'
        ]
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_license_classes', function (Blueprint $table) {
            $table->id();
            $table->string('class');
            $table->string('description');
        });
        foreach ($this->data as $d) {
            \App\Models\DriverLicenseClass::create([
                'class' => $d['class'],
                'description' => $d['description']
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_license_classes');
    }
}
