<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('park_announcements','Park\ParkAnnouncementController@index');

Route::middleware('checkType')->group(function () {

    Route::get('aggregators', 'AggregatorController@index');
    Route::get('cities', 'CityController@index');


    Route::namespace('Auth')->group(function () {
        Route::post('token', 'ApiTokenController@store'); //create token
        Route::put('token', 'ApiTokenController@refresh')->middleware('refreshToken'); //refresh token
        Route::delete('token', 'ApiTokenController@logout')->middleware('userAuth'); //delete token(logout)

        Route::post('send_sms', "SmsController@sendSms");
        Route::post('send_sms_login', "SmsController@sendSmsLogin");
    });

    Route::middleware('userAuth')->group(function () {

        Route::namespace('Driver')->group(function () {
            Route::middleware('driverAuth')->group(function () {
                Route::get('drivers', 'DriverController@index');
                Route::get('drivers/{driver}', 'DriverController@show');
                Route::put('drivers', 'DriverController@update');
                Route::put('drivers/image', 'DriverController@updateImage');

                //settings
                Route::post('driver_settings', 'DriverSettingController@store');

                //license
                Route::get('driver_license_classes', 'DriverLicenseController@index');
                Route::post('driver_license_classes', 'DriverLicenseController@store');

                //experience
                Route::get('driver_experiences_classes', 'DriverExperienceController@index');
                Route::post('driver_experiences_classes', 'DriverExperienceController@store');


                Route::get('driver_assign/{announcement_id}','DriverActionController@responseToAnnouncement');
                Route::post('rate','DriverActionController@ratePark');

            });
        });
        //parks driver
        Route::middleware('driverAuth')->group(function () {
            Route::namespace('Park')->group(function () {
                Route::get('parks','ParkController@index');
                Route::get('park/{park}','ParkController@showToDriver');
                Route::get('get_park_announcements','ParkAnnouncementController@getParkAnnouncementDriver');
            });

        });


        Route::namespace('Park')->group(function () {
            Route::middleware('parkAuth')->group(function () {

                Route::post('park_setting', 'ParkSettingsController@store');
                Route::put('park_cities','ParkSettingsController@updateCities');
                Route::put('park_aggregators','ParkSettingsController@updateAggregators');

                Route::get('park','ParkController@show');
                Route::put('park','ParkController@update');
                Route::put('park/image', 'ParkController@updateImage');

                Route::get('park_announcements','ParkAnnouncementController@index');
                Route::post('park_announcements','ParkAnnouncementController@store');
                Route::put('park_announcements/{announcement_id}','ParkAnnouncementController@update');
                Route::delete('park_announcements/{announcement_id}','ParkAnnouncementController@delete');

            });
        });


    });
});

